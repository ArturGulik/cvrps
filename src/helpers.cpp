#include "helpers.h"

template <class T, class U>
double distance(T& A, U& B){
  return sqrt((A.x-B.x)*(A.x-B.x)+(A.y-B.y)*(A.y-B.y));
}

template double distance<Point, Point>(Point&, Point&);
template double distance<Point, ExPoint>(Point&, ExPoint&);
template double distance<ExPoint, Point>(ExPoint&, Point&);
template double distance<ExPoint, ExPoint>(ExPoint&, ExPoint&);

template <class T>
vector<vector<double> > create_dist_matrix(T& points){
  vector<vector<double> > output(points.size());
  for (int p1=0; p1 < (int)points.size(); ++p1){
    output[p1] = vector<double>(points.size());
    for (int p2=0; p2 < (int)points.size(); ++p2){
      if (p1 == p2){
	output[p1][p1] = 0;
	continue;
      }
      double dist = distance(points[p1], points[p2]);
      output[p1][p2] = dist;
    }
  }
  return output;
}

template vector<vector<double> > create_dist_matrix(vector<Point>&);
template vector<vector<double> > create_dist_matrix(vector<ExPoint>&);

void clear() {
  const char *CLEAR_SCREEN_ANSI = "\e[1;1H\e[2J";
  write(STDOUT_FILENO, CLEAR_SCREEN_ANSI, 12);
}

struct termios default_termios;

void set_default_termios(){
  tcgetattr(STDIN, &default_termios);
}

void canonical_mode(bool canonical){
  if(canonical){
    // Resetting termios
    tcsetattr(STDIN, TCSANOW, &default_termios);
    return;
  }
  struct termios info;
  tcgetattr(STDIN, &info);
    
  info.c_lflag &= ~ICANON;
  info.c_cc[VMIN] = 1;
  info.c_cc[VTIME] = 0;
  tcsetattr(0, TCSANOW, &info);
}

double rand_double(double min, double max){
  return min + (double)(rand()) / ((double)(RAND_MAX/(max-min)));
}
