#include "kmeans.h"

namespace kmeans{
  int tries = 300;
  int tolerance = 1;
  int truck_inc = 5;
  int iteration_count = 100;
  int max_bruteforce = 10;
  bool display_routes = false;

  vector<Point> find_centroids(vector<Point>& points, int n){
    if ((long unsigned int)n > points.size()-1) n = points.size()-1;
    vector<Point> centroids;
    centroids.push_back(points.at(rand()%(points.size()-1)));
    while ((int)centroids.size() < n){
      vector<double> min_distances = {};
      double min_distance;
      double distance_sum = 0;
      for(vector<Point>::iterator point = points.begin();
	  point != points.end()-1; ++point){ // omit the depot
	// Find the distance to the nearest centroid
	min_distance = DBL_MAX;
	for(vector<Point>::iterator c = centroids.begin();
	    c != centroids.end(); ++c){
	  double d = distance(*c, *point);
	  if (d < min_distance)
	    min_distance = d;
	}
	min_distances.push_back(min_distance);
	distance_sum += min_distance;
      }
      int choice = rand() % ((int)distance_sum);
      double sum = -1;
      int i = 0;
      while (sum<choice){
	sum += min_distances.at(i++);
      }
      i--;
      // Point i was selected as the next centroid
      centroids.push_back(points.at(i));
    }
    return centroids;
  }

  int update_exp(vector<ExPoint>& expoints,
		 vector<Point>& centroids,
		 vector<double>& demand, double capacity){
    // update cluster membership and min distances
    
    //return value - 0 if no changes have been made
    //               otherwise number of changes
    vector<double> order_sum(centroids.size(), 0.0);
    int output = 0;
    for(vector<Point>::iterator c = centroids.begin();
	c != centroids.end(); ++c){
      for(vector<ExPoint>::iterator exp = expoints.begin();
	  exp != expoints.end(); ++exp){
	double d = distance(*exp, *c);
	if (d < (*exp).min_dist){
	  output++;
	  (*exp).min_dist = d;
	  (*exp).cluster = c-centroids.begin();
	  order_sum[(*exp).cluster] += demand[exp-expoints.begin()];
	  if (order_sum[(*exp).cluster] > capacity){
	    order_sum = vector<double>(centroids.size(), 0.0);
	  }
	}
      }
    }
    return output;
  }
  
  vector<vector<ExPoint> > kmeans(vector<Point>& centroids, vector<Point>& p,
				  vector<double>& demand, double capacity,
				  int iterations){
    vector<int> point_count(centroids.size(), 0);
    vector<Point> sum(centroids.size(), Point{0.0, 0.0});
    
    vector<ExPoint> exp(p.size()-1);
  
    int i=0;
    for(vector<Point>::iterator point = p.begin();
	point != p.end()-1; ++point){
      exp[i] = ExPoint{(*point).x, (*point).y, -1, DBL_MAX};
      ++i;
    }
  kmeans:
    update_exp(exp, centroids, demand, capacity);
    for (int i=0; i<iterations; ++i){
      point_count.resize(centroids.size(), 0);
      sum.resize(centroids.size(), Point{0.0, 0.0});
      // Calculate cluster members and coordinate sums
      for (vector<ExPoint>::iterator e = exp.begin();
	   e != exp.end(); ++e){
	point_count[e->cluster]++;
	sum[e->cluster].x += e->x;
	sum[e->cluster].y += e->y;
      }
      // Using data from the loop above, update centroid coordinates
      for (int c=0; c<(int)centroids.size(); ++c){
	if (point_count[c] == 0){
	  //centroid is not needed
	  centroids[c] = centroids.back();
	  centroids.pop_back();
	  continue;
	}
	centroids[c].x = sum[c].x / point_count[c];
	centroids[c].y = sum[c].y / point_count[c];
      }
      int changes = update_exp(exp,centroids,demand,capacity);
      if (!changes) {
	// no improvements have been made
	break;
      }
    }
    vector<double> cost(centroids.size(), 0.0);
    vector<vector<ExPoint> > output(centroids.size());
    for (vector<ExPoint>::iterator e = exp.begin();
	 e != exp.end(); ++e){
      output[e->cluster].push_back(*e);
      cost[e->cluster] += demand[e-exp.begin()];
      if (cost[e->cluster] > capacity) {
	centroids.push_back(Point{e->x, e->y});
	e->cluster = centroids.size()-1;
	goto kmeans;
      }
    }
    return output;
  }

  void print_route(vector<ExPoint>& exp){
    for (int e=0; e< (int)exp.size()-1;++e){
	std::cout<<"("<<exp[e].x<<", "<<exp[e].y<<") => ";
      }
      std::cout<<"("<<exp.back().x<<", "<<exp.back().y<<")\n";
  }
  bool includes_point(vector<ExPoint>& points, ExPoint p){
    for (int i=0;i<(int)points.size();++i){
      if (points[i].x == p.x && points[i].y == p.y)
	return true;
    }
    return false;
  }
  vector<ExPoint> bruteforce_min_route(vector<ExPoint>& points, vector<vector<double> >& dist){
    int depot = points.size()-1;
    int c = depot; // number of clients
    vector<int>order(c);
    
    if (points.size() == 2) {
      return {points[1], points[0], points[1]};
    }
    
    for (int i=0;i<c;++i){
      order[i] = i;
    }
    bool debug = false;
    if (debug) std::cout<<"Instance {\n";
    
    double min_length = DBL_MAX;
    vector<ExPoint> output(c+2);
    
    while(next_permutation(order.begin(), order.end())){
      double length = 0;
      int current = depot;
      for (int i=0;i<c;++i){
	length += dist[current][order[i]];
	current = order[i];
      }
      length += dist[current][depot];
      
      if (length < min_length){
	min_length = length;
	output[0] = points[depot];
	for (int i=0;i<c;++i){
	  output[i+1] = points[order[i]];
	}
	output[c+1] = points[depot];
	if (debug){
	  std::cout<<"Solution:\n";
	  print_route(output);
	  std::cout<<"^-- length = "<<length<<"\n";
	}
      }
    }
    if (debug){
      std::cout<<"}\n";
    }
    return output;
  }

  vector<ExPoint> min_route(vector<ExPoint>& points){
    vector<vector<double> > dist = create_dist_matrix(points);
    if ((int)points.size() <= max_bruteforce)
      return bruteforce_min_route(points, dist);
    double length = 0;
    double min = DBL_MAX;
    double min_vertex = DBL_MAX;
    bool visited[dist.size()+1];
    for(int i=0;i < (int)dist.size()+1;++i){
      visited[i] = false;
    }
    int route[dist.size()+1]; // clients + 2x depot

    // Starting from the depot
    int current = dist.size()-1;
    
    visited[current] = true;
    route[0] = current;
    
    for (int i=1;i < (int)dist.size(); ++i){
      min = DBL_MAX;
      min_vertex = -1;
      for (int next=0; next < (int)dist.size()-1;++next){
	if (current != next && !visited[next] && dist[current][next] < min){
	  min = dist[current][next];
	  min_vertex = next;
	}
      }
      current = min_vertex;
      visited[current] = true;
      route[i]=current;

      length += min;
    }
    // Going back to depot
    length += dist[current][dist.size()-1];
    route[dist.size()] = dist.size()-1;

    vector<ExPoint> output(dist.size()+1);
    for (int x=0;x < (int)dist.size()+1;++x){
      output[x] = points[route[x]];
      //std::cout<<points[route[x]].x<<" "<<points[route[x]].y<<"\n";
    }
    //std::cout<<"Min distance found is: "<<length<<"\n";
    return output;
  }

  vector<vector<ExPoint> > split_points(vector<ExPoint>& exps, Point Depot){
    int size = 5;
    vector<vector<ExPoint> > output(size);
    ExPoint depot = {Depot.x, Depot.y, 0, 0};
    for(int i=0; i< (int)output.size();++i){
      output[i].push_back(depot);
    }
    for(int i=0; i< (int)exps.size();++i){
      output[exps[i].cluster].push_back(exps[i]);
    }
    for(int i=0; i< (int)output.size();++i){
      output[i].push_back(depot);
    }
    return output;
  }

  void calc_routes(vector<vector<ExPoint> >& exps, ExPoint depot){
    for (int i=0; i< (int)exps.size(); ++i){
      exps[i].push_back(depot);
      //std::cout<<"Before routing: \n";
      //print_route(exps[i]);
      exps[i] = min_route(exps[i]);
      //std::cout<<"After routing: \n";
      //print_route(exps[i]);
    }
  }
  
  void print_routes(vector<vector<ExPoint> >& exps){
    for (int i=0; i< (int)exps.size(); ++i){
      std::cout<<"Route "<<i<<":\n";
      print_route(exps[i]);
    }
  }
  double get_distance(vector<vector<ExPoint> >& exps){
    double out = 0;
    for (int i=0; i< (int)exps.size(); ++i){
      for (int e=0; e< (int)exps[i].size()-1;++e){
	double d = distance(exps[i][e], exps[i][e+1]);
	out += d;
      }
    }
    return out;
  }

  vector<vector<ExPoint> > solve(vector<Point>& points, vector<double>& demand, double capacity){
    ExPoint depot = {points[points.size()-1].x, points[points.size()-1].y, 0, 0.0};
    int demandSum=0;
    double minDist = DBL_MAX;
    vector<vector<ExPoint> > bestSolution(0);
    for (int i=0; i< (int)demand.size(); ++i){
      demandSum += demand[i];
    }
    int minTrucks = demandSum/capacity;
    int maxTrucks = minTrucks + truck_inc;
    int notfound = 0;
    for (int centroidCount = minTrucks; centroidCount <= std::min(maxTrucks, (int)points.size()-1); ++centroidCount){
      bool found = false;
      for (int i=0; i<tries;++i){
	vector<Point> centroids = find_centroids(points, centroidCount);
	vector<vector<ExPoint> > routes = kmeans(centroids, points, demand, capacity, iteration_count);
	if (!(int)routes.size()) continue;
	found = true;
	calc_routes(routes, depot);
	double dist = get_distance(routes);
	if (dist < minDist){
	  minDist = dist;
	  bestSolution = routes;
	}
      }
      if (!found){
	notfound++;
	if (notfound == tolerance){
	  // No solutions were found in `tolerance` iterations
	  notfound = 0;
	  centroidCount+=truck_inc;
	  maxTrucks = centroidCount + truck_inc;
	}
      }   
    }
    if (display_routes)
      print_routes(bestSolution);
    
    return bestSolution;
  }

  int get_tries(){
    return tries;
  }
  int get_tolerance(){
    return tolerance;
  }
  int get_truck_inc(){
    return truck_inc;
  }
  int get_iteration_count(){
    return iteration_count;
  }
  int get_max_bruteforce(){
    return max_bruteforce;
  }
  bool get_display_routes(){
    return display_routes;
  }

  void set_tries(int t){
    tries = t;
  }
  void set_tolerance(int t){
    tolerance = t;
  }
  void set_truck_inc(int t){
    truck_inc = t;
  }
  void set_iteration_count(int t){
    iteration_count = t;
  }
  void set_max_bruteforce(int t){
    max_bruteforce = t;
  }
  void set_display_routes(bool t){
    display_routes = t;
  }
}
