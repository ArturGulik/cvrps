#ifndef INTERFACE_H
#define INTERFACE_H

#include <vector>
#include <cmath>
#include <ctime>
#include <cstdio>
#include <string>
#include <filesystem>

#include "helpers.h"
#include "Point.h"
#include "solver.h"
#include "io.h"

#define VERSION "1.0"
#define YEARS 2022
#define concat(one, two, three) one two three

using std::vector;
using std::string;
namespace fs = std::filesystem;
namespace interface{
  int process_flags(int, char**);
  void loop();
}
#endif
