#ifndef IO_H
#define IO_H

#include <iostream>
#include <string>
#include <vector>
#include <fstream>
#include <nlohmann/json.hpp>

#include "helpers.h"
#include "Point.h"

using json = nlohmann::json;
using std::string;
using std::vector;

namespace exportd{
  void points(vector<Point>&, string);
  void points(vector<ExPoint>&, string);
  void routes(vector<vector<int>>&, vector<Point>&, int depot);
}

#endif
