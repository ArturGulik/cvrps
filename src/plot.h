#ifndef PLOT_H
#define PLOT_H

#include <string>

using std::string;

namespace plot{
  extern string command;
  extern string data_path;
  void clear();
  void add(string, string);
  void plot();
}

#endif
