#ifndef HELPERS_H
#define HELPERS_H

#include "Point.h"
#include <vector>
#include <cmath>
#include <ctime>
#include <unistd.h>
#include <termios.h>

using std::vector;

#define STDIN 0
#define foreach(i, X) for (int i=0;i<X;++i)
#define eachfor(i, X) for (int i=X-1;i>-1;--i)

template <class T, class U>
double distance(T&, U&);

template <class T>
vector<vector<double> > create_dist_matrix(T&);

void clear();

extern struct termios default_termios;
void set_default_termios();
void canonical_mode(bool);

double rand_double(double, double);
#endif
