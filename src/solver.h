#ifndef SOLVER_H
#define SOLVER_H
#include <iostream>
#include <cstdio>
#include <cmath>
#include <string>
#include <vector>
#include <set>

#include "Point.h"
#include "kmeans.h"
#include "helpers.h"
#include "plot.h"
#include "io.h"

using std::string;
using std::vector;
using std::set;

namespace solver{
  extern string datasetName;
  void test_solver();
  void import_problem(string);
  string get_dataset();
  void generate_data(int, double, double, double);
  void inspect_data();
}

#endif
