#include "interface.h"

int main(int argc, char* argv[]){
  if (interface::process_flags(argc, argv)) return 0;
  
  interface::loop();
      
  return 0;
}
