#include "interface.h"

namespace interface{
  void version(){
    printf("  ______     ______  ____  ____  \n");
    printf(" / ___\\ \\   / /  _ \\|  _ \\/ ___| \n");
    printf("| |    \\ \\ / /| |_) | |_) \\___ \\ \n");
    printf("| |___  \\ V / |  _ <|  __/ ___) |\n");
    printf(" \\____|  \\_/  |_| \\_\\_|   |____/ \n");
    std::cout<<"Capacitated Vehicle Routing Problem Solver version "<<VERSION<<"\n";
    std::cout<<"Copyright Artur Gulik "<<YEARS<<" (C)\n";
    printf("License: GNU GPL version 3 <https://www.gnu.org/licenses/gpl-3.0.html>\n");
    printf("This is free software: you are free to change and redistribute it.\n");
    printf("There is NO WARRANTY, to the extent prermitted by law.\n");
  }

  void help(){
      printf("Usage:\n");
      printf("\tcvrps [options]\n\n");
      
      printf("Options:\n");
      printf("\t-h, --help\t\tDisplay this help information and exit\n");
      printf("\t-v, --version\t\tDisplay version information and exit\n");
  }

  int process_flags(int argc, char* argv[]){
    for (int i=1; i < argc; ++i){
      string arg(argv[i]);
      if (arg == "-h" || arg == "--help"){
      	interface::help();
	return 1;
      }
      if (arg == "-v" || arg == "--version"){
	interface::version();
	return 1;
      }
      std::cout<<"Unrecognized option: "<<arg<<"\n";
      return 1;
    }
    return 0;
  }

  void solver_settings(){
    char input;
    int tries = kmeans::get_tries();
    int tolerance = kmeans::get_tolerance();
    int truck_inc = kmeans::get_truck_inc();
    int iteration_count = kmeans::get_iteration_count();
    int max_bruteforce = kmeans::get_max_bruteforce();
    bool display_routes = kmeans::get_display_routes();
    while(1){
      clear();
      printf("Solver settings\n");
      printf("1. Tries per truck count. Current: %d\n", tries);
      printf("2. Number of iterations without solutions before truck count is increased. Current: %d\n", tolerance);
      printf("3. Truck count increase value after no solutions are found in %d iterations. Current: %d\n", tolerance, truck_inc);
      printf("4. Maximum iteration count for the clustering algorithm. Current: %d\n", iteration_count);
      printf("5. Maximum client count for the bruteforce algorithm (above that amount the greedy algorithm is used). Current: %d\n", max_bruteforce);
      printf("6. Display routes after solving: "); std::cout<<((display_routes)? "YES" : "NO") <<"\n";
      printf("0. Back\n");
      input = getchar();
      fflush(stdin);
      switch(input){
      case '1':
	clear();
	std::cout<< "Setting tries per truck count. Current: " << tries << "\n";
	std::cout<< "New value: ";
	std::cin >> tries;
	kmeans::set_tries(tries);
	break;
      case '2':
	clear();
	std::cout<< "Setting number of iterations before truck count is increased. Current: " << tolerance << "\n";
	std::cout<< "New value: ";
	std::cin >> tolerance;
	kmeans::set_tolerance(tolerance);
	break;
      case '3':
	clear();
	std::cout<< "Truck count increase. Current: " << truck_inc << "\n";
	std::cout<< "New value: ";
	std::cin >> truck_inc;
	kmeans::set_truck_inc(truck_inc);
	break;
      case '4':
	clear();
	std::cout<< "Setting iteration count for the clustering algorithm. Current: " << iteration_count << "\n";
	std::cout<< "New value: ";
	std::cin >> iteration_count;
	kmeans::set_iteration_count(iteration_count);
	break;
      case '5':
	clear();
	std::cout<< "Setting maximum number of clients for the bruteforce algorithm. Current: " << max_bruteforce << "\n";
	std::cout<< "New value: ";
	std::cin >> max_bruteforce;
	kmeans::set_max_bruteforce(max_bruteforce);
	break;
      case '6':
	display_routes = !display_routes;
	kmeans::set_display_routes(display_routes);
	break;
      case '0':
	return;
      }
    }
  }
  
  void data_generator_menu(){
    char input;
    int clientCount = 30;
    double quantity = 15.0;
    double capacity = 100.0;
    while(1){
      clear();
      printf("Data generator\n");
      printf("1. Set client count. Current: %d\n", clientCount);
      std::cout<<"2. Set average order quantity. Current: " << quantity << "\n";
      std::cout<<"3. Set truck capacity. Current: " << capacity << "\n\n";
      std::cout<<"4. Generate data\n\n";
      printf("0. Cancel\n");
      input = getchar();
      fflush(stdin);
      switch(input){
      case '1':
	clear();
	std::cout<< "Setting client count. Current: " << clientCount << "\n";
	std::cout<< "New client count: ";
	std::cin >> clientCount;
	break;
      case '2':
	clear();
	std::cout<< "Setting average order quantity. Current: " << quantity << "\n";
	std::cout<< "New quantity: ";
	std::cin >> quantity;
	break;
      case '3':
	clear();
	std::cout<< "Setting truck capacity. Current: " << capacity << "\n";
	std::cout<< "New capacity: ";
	std::cin >> capacity;
	break;
      case '4':
	clear();
	solver::generate_data(clientCount, quantity, capacity, 100.0);
	std::cout<< "Data generated successfully!\n";
	printf("Press any key to continue...");
	input = getchar();
	fflush(stdin);
	return;
      case '0':
	return;
      }
    }
  }
  void data_menu(){
    char input;
    while(1){
      clear();
      printf("Data settings\n");
      std::cout<<"Current dataset: " << solver::get_dataset() << "\n";
      printf("1. Browse datasets\n");
      std::cout<<"2. Data generator\n";
      std::cout<<"3. Inspect current data\n\n";
      printf("0. Back to main menu\n");
      input = getchar();
      fflush(stdin);
      switch(input){
      case '1': {
	clear();
	int i=1;
	string datafiles[100];
	int datafileCount = 0;
	for (const auto & entry : fs::directory_iterator("data")){
	  std::string filename = string(entry.path()).substr(string(entry.path()).find_last_of("/") + 1);
	  std::cout << i << ". " << filename << "\n";
	  i++;
	  datafiles[datafileCount++] = entry.path();
	}
	std::cout << "0. Cancel\n";
	input = getchar();
	fflush(stdin);
	clear();
	if (input == '0') break;
	if (input > '0' && (input - '0') <= i-1){
	  std::cout << "Loading problem data from " << datafiles[input - '0' - 1] << "\n";
	  solver::import_problem(datafiles[input - '0' - 1]);
	}
	
	printf("Press any key to continue...");
	input = getchar();
	fflush(stdin);
	break;
      }
      case '2':
	data_generator_menu();
	break;
      case '3':
	clear();
	solver::inspect_data();
	printf("Press any key to continue...");
	input = getchar();
	fflush(stdin);
	break;
      case '0':
	return;
      }
    }
  }
  void loop(){
    solver::import_problem("data/CandE1969_E_n022_k04.json");
    set_default_termios();
    canonical_mode(false);
    srand(time(NULL));
    char input;
    while(1){
      clear();
      printf("CVRP Solver\n");
      printf("1. Run solver\n");
      std::cout<<"2. Data settings. Current: " <<solver::get_dataset() << "\n";
      std::cout<<"3. Solver settings\n\n";
      printf("0. Exit the program\n");
      input = getchar();
      fflush(stdin);
      switch(input){
      case '1':
	clear();
	solver::test_solver();
	printf("Press any key to continue...");
	input = getchar();
	fflush(stdin);
	break;
      case '2':
	data_menu();
	break;
      case '3':
	clear();
	solver_settings();
	break;
      case '0':
	clear();
	printf("Are you sure you want to exit? [y/N]\n");
	input = getchar();
	fflush(stdin);
	if (input == 'y' || input == 'Y'){
	  clear();
	  canonical_mode(true);
	  return;
	}
	break;
      }
    }
  }
}
