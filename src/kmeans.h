#ifndef KMEANS_H
#define KMEANS_H

#include <iostream>
#include <cfloat>
#include <vector>
#include <map>
#include <algorithm>

#include "Point.h"
#include "helpers.h"
using std::vector;
using std::string;
using std::map;

namespace kmeans{
  vector<Point> find_centroids(vector<Point>&, int);
  vector<vector<ExPoint> > kmeans(vector<Point>&, vector<Point>&,
				  vector<double>&, double, int);
  vector<ExPoint> min_route(vector<ExPoint>&);
  vector<vector<ExPoint> > split_points(vector<ExPoint>&, Point);
  double get_distance(vector<vector<ExPoint> >&);
  void calc_routes(vector<vector<ExPoint> >&, ExPoint);
  vector<vector<ExPoint> > solve(vector<Point>&, vector<double>&, double);
  
  int get_tries();
  int get_tolerance();
  int get_truck_inc();
  int get_iteration_count();
  int get_max_bruteforce();
  bool get_display_routes();

  void set_tries(int);
  void set_tolerance(int);
  void set_truck_inc(int);
  void set_iteration_count(int);
  void set_max_bruteforce(int);
  void set_display_routes(bool);
}

#endif
