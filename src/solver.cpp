#include "solver.h"

int C = 5;
int depot = C;
double capacity = 4;
vector<Point> Cs;
vector<double> demand(C);
vector<int> pred(C);
vector<bool> last(C);

vector<vector<int>> s_routes;

void print_route(vector<int> route){
  int x = route.size();
  printf("DEPOT ->");
  eachfor(c, x){
    //if (c == x-1) printf(" ");
    if (route.at(c) == depot){
      printf("DEPOT FOUND");
      exit(0);
    }
    printf(" %d ->", route.at(c));
  }
  printf(" DEPOT\n");
}

/**
   Calculate how many goods were delivered by a vehicle from its route
*/
int left_goods(vector<int> route){
  int x = route.size();
  int load = capacity;
  //printf("Demands: ");
  eachfor(c,x){
    //printf("%d (%d), ", demand[route.at(c)], route.at(c));
    load -= demand[route.at(c)];
  }
  //printf("\n");
  return load;
}


void print_routes(vector<vector<int>> routes){
  int x = routes.size();
  foreach(v, x){
    printf("Vehicle %d: ", v);
    print_route(routes.at(v));
    printf("Left goods: %d\n", left_goods(routes.at(v)));
  }
}

/**
   Find the nearest neighbor of a customer
 */
int nearest_neighbor(int customer){
  return 0;
}

namespace solver {
  string datasetName = "None";
  
  /**
     Calculate whole distance travelled in a solution
  */
  double s_distance(){
    double total = 0;
    foreach(c,C){
    total += distance(Cs[c], Cs[pred[c]]);
    if (last[c])
      total += distance(Cs[c], Cs[depot]);
    }
    return total;
  }
  
  /**
     Calculate how many vehicles were sent in a solution
  */
  int s_sent_vehicle_count(){
    int total = 0;
    foreach(x,C){
      if (pred[x] == depot)
	++total;
    }
    return total;
  }

  /**
     Calculate how many vehicles returned in a solution
  */
  int s_returned_vehicle_count(){
    int total = 0;
    foreach(c,C){
      if (last[c])
	++total;
    }
    return total;
  }
  
  vector<vector<int>> s_get_routes() {
    vector<vector<int>> routes = {};
    set<int> client_set = {};
    foreach(c,C){
      if (!last[c]) continue;
      vector<int> route = {};
      int i = c;
      //printf(" %d ", i);
      do {
	if (client_set.find(i) != client_set.end())
	  return {};
	client_set.insert(i);
	route.push_back(i);
	i = pred[i];
      } while (i != depot);
      routes.push_back(route);
    }
    if ((int)client_set.size() != C) return {};
    return routes;
  }
  
  bool check_solution(){
    int x = s_routes.size();
    //printf("    Passed 0\n");
    if (x == 0) return false;
    //printf("    Passed 1\n");
    if (s_sent_vehicle_count() != s_returned_vehicle_count())
      return false;
    foreach(r, x){
      if (left_goods(s_routes.at(r)) < 0) return false;
    }
    return true;
  }
  
  int get_state(int base, int id, int element){
    return id/((int)pow(base, element)) % base;
  }
  
  void print_variables(){
    foreach(x, C){
      //printf("  pred[%d]: %d\n", x, pred[x]);
    }
    foreach(y, C){
      //printf("  last[%d]: %d\n", y, last[y]);
    }
  }
  void test_solver(){
    vector<vector<ExPoint> > solution = kmeans::solve(Cs, demand, capacity);
    if (solution.size() == 0){
      printf("No solutions found. Try adjusting solver settings.\n");
      return;
    }
    double minDist = kmeans::get_distance(solution);
    printf("Solution: %f\n", minDist);

    plot::clear();
    for (int i=0;i<(int)solution.size();++i){
      exportd::points(solution[i], "Cluster" + std::to_string(i));
      exportd::points(solution[i], "Cluster" + std::to_string(i));
      plot::add("Cluster" + std::to_string(i), "w l notitle");
      plot::add("Cluster" + std::to_string(i), "w p pt 7 ps 1 lc variable notitle");
    }
    //exportd_points(&route, "route");
    //plot::add("route", "w l t 'route'");

    vector<Point> Depot(1);
    Depot[0] = Cs[Cs.size()-1];
    Cs.pop_back();
    exportd::points(Depot, "Depot");
    exportd::points(Cs, "Clients");
    Cs.push_back(Depot[0]);

    plot::add("Depot", "w p pt 7 ps 1 lc variable t 'Clients'");//legend entry for clients
    plot::add("Depot", "w p pt 7 ps 2 lc black t 'Depot'");
        
    plot::plot();
    
  }
  void brute_force(){    
    int best_id = -1;
    int best_id2 = -1;
    int best_distance = 999999999;
    foreach(id, pow(C+1, C)){
      foreach(x, C){
	pred[x] = get_state(C+1, id, x);
      }
      foreach(id2, pow(2,C)){
	foreach(y, C){
	  last[y] = get_state(2, id2, y);
	}
      // Solution state set
      //printf("[%d:%d]", id, id2);//id:
      //print_variables();
	s_routes = s_get_routes();
	if (check_solution()){
	//printf("id: %d:%d\n", id, id2);
	//print_variables();
	//print_routes(s_routes);
	//printf("Distance: %f\n", s_distance());
	  if (s_distance() < best_distance){
	    best_distance = s_distance();
	    best_id = id;
	    best_id2 = id2;
	  }
	}
      }
    }
    // RESTORE BEST SOLUTION
    foreach(x, C){
      pred[x] = get_state(C+1, best_id, x);
    }
    foreach(y, C){
      last[y] = get_state(2, best_id2, y);
    }
    s_routes = s_get_routes();
    printf("id: %d:%d\n", best_id, best_id2);
    print_variables();
    print_routes(s_routes);
    printf("Distance: %f\n", s_distance());
    exportd::routes(s_routes, Cs, depot);
  }

  string get_dataset(){
    return datasetName;
  }

  void import_problem(string filepath){
    std::ifstream datafile(filepath);
      json data = json::parse(datafile);
      data = data["instance"];
      datasetName = data["info"]["dataset"].get<string>();
      //std::cout<<"Dataset: "<<data["info"]["dataset"]<<"\n";
      json clients = data["network"]["nodes"]["node"];
      json requests = data["requests"]["request"];
      double depotX = atof(clients[0]["x"].get<string>().c_str());
      double depotY = atof(clients[0]["y"].get<string>().c_str());
      demand = vector<double>(requests.size());
      Cs = {};
      for(int client=1; client<(int)clients.size(); ++client){
	Point a = {atof(clients[client]["x"].get<string>().c_str()),
		   atof(clients[client]["y"].get<string>().c_str())};
	Cs.push_back(a);
	//printf("%d: %f\n", client-1, atof(requests[client-1]["quantity"].get<string>().c_str()));
	if (client-1 < (int)requests.size())
	  demand[client-1] = atof(requests[client-1]["quantity"].get<string>().c_str());
      }
      Point depotP = {depotX, depotY};
      Cs.push_back(depotP);
      capacity = atof(data["fleet"]["vehicle_profile"]["capacity"].get<string>().c_str());
  }

  void generate_data(int clientCount, double quantity, double cap, double world_size){
    double maxDiff = 0.2;
    Cs = vector<Point>(clientCount+1);
    demand = vector<double>(clientCount);
    for (int i=0; i<clientCount; ++i){
      Point a = {rand_double(-world_size, world_size), rand_double(-world_size, world_size)};
      Cs[i] = a;
      demand[i] = rand_double(quantity*(1-maxDiff), quantity*(1+maxDiff));
    }
    capacity = cap;
    Cs[clientCount] = {0, 0}; //depot init
    datasetName = "Custom generated";
  }

  void inspect_data(){
    std::cout<<"Dataset: " << datasetName << "\n\n";
    
    std::cout<<"Client count: " << Cs.size()-1 << " | Truck capacity: " << capacity << "\n";

    double sum = 0;
    for (int i=0; i< (int)demand.size(); ++i){
      sum += demand[i];
    }
    double avrg = sum/demand.size();
    std::cout<<"Average order quantity: " << avrg << "\n";
    
    plot::clear();
    vector<Point> Depot(1);
    Depot[0] = Cs[Cs.size()-1];
    Cs.pop_back();
    exportd::points(Depot, "Depot");
    exportd::points(Cs, "Clients");
    Cs.push_back(Depot[0]);
    plot::add("Depot", "w p pt 7 ps 2 lc black t 'Depot'");
    plot::add("Clients", "w p pt 7 ps 1 lc variable t 'Clients'");
    plot::plot();
  }
}
