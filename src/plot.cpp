#include "plot.h"

namespace plot {
  string data_path = "tmp/gnuplot_data/";
  string command = "gnuplot -p -e \""
    "set style line 1 lt 1 lw 1 lc black;"
    "set key left top;"
    "set key box linestyle 1;"
    "set key spacing 2;"
    "set size ratio -1;"
    "set offset graph 0.1, graph 0.1, graph 0.2, graph 0.1;"
    "plot ";


  void clear(){
    command = "gnuplot -p -e \""
    "set style line 1 lt 1 lw 1 lc black;"
    "set key left top;"
    "set key box linestyle 1;"
    "set key spacing 2;"
    "set size ratio -1;"
    "set offset graph 0.1, graph 0.1, graph 0.2, graph 0.1;"
    "plot ";
  }
  
  void add(string file_name, string options){
    command += "'" + data_path + file_name + "'" + " " + options + ", ";
  }
  
  void plot(){
    command[command.size()-2]='"';
    command[command.size()-1]='\0';
    //printf("%s\n", command.c_str());
    system(command.c_str());
  }
}
