#include "io.h"

namespace exportd{
  void points(vector<Point>& points, string filename){
    const string path = "tmp/gnuplot_data/" + filename;
    std::ofstream points_file;
    points_file.open(path, std::ios::out | std::ios::trunc);
    for (auto point = points.begin();
	 point != points.end(); ++point){
      points_file << point->x << " " << point->y;
      points_file << " " << point - points.begin() << "\n";
    }
    points_file.close();
  }
  
  void points(vector<ExPoint>& points, string filename){
    const string path = "tmp/gnuplot_data/" + filename;
    std::ofstream points_file;
    points_file.open(path, std::ios::out | std::ios::trunc);
    for (auto point = points.begin();
	 point != points.end(); ++point){
      points_file << point->x << " " << point->y;
      points_file << " " << point->cluster << "\n";
    }
    points_file.close();
  }

  void routes(vector<vector<int>>& routes, vector<Point>& Cs, int depot){
    int R = routes.size();
    int C;
    vector<int>* route;
    std::ofstream routes_file;
    const string path = "tmp/gnuplot_data/routes.dat";
    routes_file.open(path, std::ios::out | std::ios::trunc);
    foreach(r, R){
      route = &routes.at(r);
      C = route->size();
      routes_file << Cs[depot].x << " " << Cs[depot].y << "\n";
      eachfor(c, C){
      routes_file << Cs[route->at(c)].x << " " << Cs[route->at(c)].y << "\n";
      }
      routes_file << Cs[depot].x << " " << Cs[depot].y << "\n";
      if (r-1 != R) routes_file << "\n";
    }
    
    routes_file.close();
    string command = "gnuplot -p -e \"stats '" + path;
    command += "'; plot for [i=0:int(STATS_blank)] '" + path;
    command += "' every :::i::(i+1) with lp title 'Route '.(i) linecolor i*15;\" 2> output";
    //std::cout<<command<<"\n";
    system(command.c_str());
  }
}
