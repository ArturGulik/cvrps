#ifndef POINT_H
#define POINT_H

typedef struct Point Point;
struct Point{
  double x;
  double y;
};

typedef struct ExPoint ExPoint;
struct ExPoint{
  double x;
  double y;
  int cluster;
  double min_dist;
};

#endif
