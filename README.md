# Capacitated Vehicle Routing Problem Solver

# Getting started
You can issue the following commands for a quick start:

```bash
git clone git@gitlab.com:ArturGulik/cvrps.git
cd cvrps
make
```
# Licensing
CVRPS is released under the [GNU GPLv3](https://gitlab.com/ArturGulik/cvrps/-/blob/main/LICENSE).
