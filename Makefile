MAJOR=0
MINOR=1
VERSION=$(MAJOR).$(MINOR)

#-----------------------------------
# Paths for source and object files
S=src
O=$S/obj

#---------------
# Compiler and compiler flags
CC=g++
CFLAGS=-Wall -g
OBJ_CFLAGS=-c $(CFLAGS)

.PHONY: test clean

test: vrps
	@./cvrps

vrps: $O/io.o $O/helpers.o $O/plot.o $O/kmeans.o $O/solver.o $O/interface.o $O/main.o
	@$(CC) $(CFLAGS) $^ -o cvrps

$O/main.o: $O/helpers.o $O/solver.o $S/main.cpp
	@$(CC) $(OBJ_CFLAGS) $S/main.cpp -o $O/main.o

$O/solver.o: $S/Point.h $S/solver.cpp $S/solver.h
	@$(CC) $(OBJ_CFLAGS) $S/solver.cpp -o $O/solver.o

$O/kmeans.o: $S/helpers.h $S/Point.h $S/kmeans.cpp $S/kmeans.h
	@$(CC) $(OBJ_CFLAGS) $S/kmeans.cpp -o $O/kmeans.o

$O/helpers.o: $S/Point.h $S/helpers.cpp $S/helpers.h
	@$(CC) $(OBJ_CFLAGS) $S/helpers.cpp -o $O/helpers.o

$O/plot.o: $S/plot.h $S/plot.cpp
	@$(CC) $(OBJ_CFLAGS) $S/plot.cpp -o $O/plot.o

$O/io.o: $S/io.h $S/io.cpp
	@$(CC) $(OBJ_CFLAGS) $S/io.cpp -o $O/io.o

$O/interface.o: $S/interface.h $S/interface.cpp
	@$(CC) $(OBJ_CFLAGS) $S/interface.cpp -o $O/interface.o

clean:
	rm -f vrps $O/*.o
